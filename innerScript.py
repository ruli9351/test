#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import subprocess
import json
import sys
import time

path = "/home/ubuntu/"
gethDatadir = "data0/"
genesis_url = "https://bitbucket.org/ruli9351/test/raw/03555d933121f034a972a2e75e6925bb3a2367ef/genesis.json"


###install geth
def intall_geth():
	cmd = "sudo apt-get install -y software-properties-common"
	result = subprocess.call(cmd, shell=True)
	if result != 0:
		return 0
	cmd = "sudo add-apt-repository -y ppa:ethereum/ethereum"
	result = subprocess.call(cmd, shell=True)
	if result != 0:
		return 0
	cmd = "sudo apt-get update"
	result = subprocess.call(cmd, shell=True)
	if result != 0:
		return 0
	cmd = "sudo apt-get install -y ethereum"
	result = subprocess.call(cmd, shell=True)
	if result != 0:
		return 0
	return "install sucessful!"


### build datadir for geth
def build_dir():	
	cmd = "mkdir " + path + gethDatadir
	result = subprocess.call(cmd, shell=True)
	if result == 0:
		return "build data0 sucessful!"
	else:
		return 0


### download genesis.json
def download_genesis():
	cmd = "wget -N --no-check-certificate " + genesis_url
	result = subprocess.call(cmd, shell=True)
	if result == 0:
		return "download gensis.json sucessfull!"
	else:
		return 0


### geth init
def geth_init():
	cmd = "geth --datadir " + path + gethDatadir + " init genesis.json"
	result = subprocess.call(cmd, shell=True)
	if result == 0:
		return "geth init sucessfull!"
	else:
		return 0


###geth deamon
def geth():
	time.sleep(2)
	name = "TestNode"
	rpcport = "8545"
	port = "30303"
	cmd = "nohup geth --identity " + name + " --rpc --rpcport " + rpcport + " --datadir " + path + gethDatadir +" --port " + port + " --nodiscover >/dev/null 2>&1 &"
	result = subprocess.call(cmd, shell=True)
	if result == 0:
		return "start geth successful!"
	else:
		return 0

###get static node
def get_static_node():
	###get enode
	ipc_cmd = '{"jsonrpc":"2.0","method":"admin_nodeInfo","params":[],"id":1}'
	cmd = "echo '" + str(ipc_cmd) + "' | nc -U " + path + gethDatadir + "geth.ipc"
	result = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
	endoe_info = json.loads(str(result.stdout.read())[2:-3])
	static_node_info = []
	static_node_info.append(endoe_info['result']['enode'])
	if static_node_info != []:
		return static_node_info
	else:
		return 0

###start miner
def miner_start():
	ipc_cmd = '{"jsonrpc":"2.0","method": "miner_start", "params": [],"id":1}'
	cmd = "echo '" + str(ipc_cmd) + "' | nc -U " + path + gethDatadir + "geth.ipc"
	result = subprocess.call(cmd, shell=True)
	if result == 0:
		return "miner start!"
	else:
		return 0

###start miner
def new_account():
	ipc_cmd = '{"jsonrpc":"2.0","method": "personal_newAccount", "params": [""],"id":1}'
	cmd = "echo '" + str(ipc_cmd) + "' | nc -U " + path + gethDatadir + "geth.ipc"
	result = subprocess.call(cmd, shell=True)
	if result == 0:
		return "new account!"
	else:
		return 0

###kill geth
def kill_geth():
	cmd = "pkill geth"
	result = subprocess.call(cmd, shell=True)
	if result == 0:
		return "kill geth"
	else:
		return 0


###replace # with "
def replace():
	f = open(path + gethDatadir + "geth/static-nodes.json", "r")
	s = f.read()
	f.close()
	ewq = s.replace("#", "\"")
	f = open(path + gethDatadir + "geth/static-nodes.json", "w")
	f.write(ewq)
	f.close()
	return "replace successful!"


if sys.argv[1] == "geth":
	print(geth())
elif sys.argv[1] == "static":
	print(get_static_node())
elif sys.argv[1] == "install":
	print(intall_geth())
elif sys.argv[1] == "download":
	print(download_genesis())
elif sys.argv[1] == "init":
	print(geth_init())
elif sys.argv[1] == "dir":
	print(build_dir())
elif sys.argv[1] == "kill":
	print(kill_geth())
elif sys.argv[1] == "replace":
	print(replace())
elif sys.argv[1] == "miner":
	print(miner_start())
elif sys.argv[1] == "new":
	print(new_account())
else:
	print("please input!")
